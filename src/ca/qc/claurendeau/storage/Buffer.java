package ca.qc.claurendeau.storage;

import java.util.Random;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import ca.qc.claurendeau.exception.BufferIndexOutOfBoundsException;

public class Buffer {

	private static final String VALEUR_NULL_TO_STRING = "null";
	private static final String DELIMITEUR_TO_STRING = " , ";
	private static final String DELIMITEUR_FERMANT = "]";
	private static final String DELIMITEUR_OUVRANT = "[";
	
	private Element firstElement;
	private int capacity;

	public Buffer(int capacity) {
		this.capacity = capacity;
	}

	public int capacity() {
		return capacity;
	}
	
	public synchronized void addElement(Element element) throws BufferFullException // throws BufferFullException
	{
		if (isFull()) {
			throw new BufferFullException();
		}

		if (firstElement == null) {
			firstElement = element;
		} else {
			findLastElement().setNext(element);
		}
	}
	
	public synchronized Element removeElement() throws BufferEmptyException {
		if (firstElement == null) {
			throw new BufferEmptyException();
		}

		Element tmp = firstElement;
		firstElement = firstElement.next();
		return tmp;
	}
	
	public int getCurrentLoad() {
		Element tmp = firstElement;
		int cpt = 0;
		while (tmp != null) {
			tmp = tmp.next();
			cpt++;
		}

		return cpt;
	}

	public boolean isEmpty() {
		return firstElement == null;
	}

	public boolean isFull() {
		return getCurrentLoad() == capacity;
	}
	
	public Element at(int pos) throws BufferIndexOutOfBoundsException {
		if (pos >= getCurrentLoad()) {
			throw new BufferIndexOutOfBoundsException();
		}

		Element temp = firstElement;
		int cpt = 0;
		while (cpt != pos) {
			temp = temp.next();
			cpt++;
		}

		return temp;
	}

	public void fill() {
		Element[] elements = new Element[capacity];

		createNewElements(elements);
		setNextElementsOfEach(elements);

		firstElement = elements[0];
	}

	private void setNextElementsOfEach(Element[] elements) {
		for (int i = 0; i < capacity - 1; i++) {
			elements[i].setNext(elements[i + 1]);
		}
	}

	private void createNewElements(Element[] elements) {
		for (int i = 0; i < capacity; i++) {
			elements[i] = new Element(new Random().nextInt());
		}
	}

	private Element findLastElement() {
		if (firstElement == null) {
			return null;
		}

		Element tmp = firstElement;
		while (tmp.next() != null) {
			tmp = tmp.next();
		}

		return tmp;
	}	
	
	public String toString() {
		Element tmp = firstElement;
		String bufferString = DELIMITEUR_OUVRANT + getBufferStringContent(tmp) + DELIMITEUR_FERMANT;
		return bufferString;
	}
		
	private String getBufferStringContent(Element tmp) {
		String retour = "";
		for (int i = 0; i < capacity; i++) {
			retour += getElementToAddToBufferString(tmp, capacity, i);
			tmp = getNextTmp(tmp);
		}

		return retour;
	}

	private String getElementToAddToBufferString(Element tmp, int capacity, int currentPos) {
		return (tmp != null ? tmp.getData() : VALEUR_NULL_TO_STRING) + (currentPos < capacity - 1 ? DELIMITEUR_TO_STRING : "");
	}

	private Element getNextTmp(Element tmp) {
		return tmp != null ? tmp.next() : tmp;
	}

}
