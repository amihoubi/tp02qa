package ca.qc.claurendeau.exception;

/**
 * L'exception BufferEmptyException permet d'indiquer qu'il n'y a aucun élément
 * à l'index indiqué par l'utilisateur et que par conséquent, il ne peut y
 * accéder
 * 
 * @author Amina Mihoubi
 */
public class BufferIndexOutOfBoundsException extends Exception {

	private static final long serialVersionUID = 4464569638231285264L;
	private static final String ERROR_MESSAGE = "Vous ne pouvez pas accéder à un élément si l'index entré est supérieur à la quantité d'éléments que contient le buffer";

	public BufferIndexOutOfBoundsException() {
		super(ERROR_MESSAGE);
	}

}
