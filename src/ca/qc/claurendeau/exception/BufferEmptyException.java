package ca.qc.claurendeau.exception;

/**
 * L'exception BufferEmptyException permet d'indiquer qu'il n'y a aucun élément
 * dans le buffer et que, par conséquent, aucun élément ne peut être retiré
 * 
 * @author Amina Mihoubi
 */
public class BufferEmptyException extends Exception {
	private static final long serialVersionUID = -5817551533855897656L;
	private static final String ERROR_MESSAGE = "Vous ne pouvez pas retirer d'élément à un buffer vide ";

	public BufferEmptyException() {
		super(ERROR_MESSAGE);
	}

}
