package ca.qc.claurendeau.exception;

/**
 * L'exception BufferFullException permet d'indiquer que le buffer est déjà
 * rempli et que, par conséquent, aucun élément ne peut lui être ajouté
 * 
 * @author Amina Mihoubi
 */
public class BufferFullException extends Exception {
	private static final long serialVersionUID = -5244712183993434093L;
	private static final String ERROR_MESSAGE = "Vous ne pouvez pas ajouter d'élément à un buffer plein ";

	public BufferFullException() {
		super(ERROR_MESSAGE);
	}

}
